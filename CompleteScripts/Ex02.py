# Objective: Ask user's name and then say hello:

# Use raw_input to prompt user for input and save the answer in variable nm
nm=input('What is your name?\n')

# Format the name as a string "%s" and print to the screen
print("Hello %s" % (nm))
