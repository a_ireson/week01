# Objective is to sum the numbers stored in a list

# Create the list
data=[1,-8,2,-6,3]

# Print the sum of the list:

# Method 1 - use the build in function "sum"
print("Sum, using method 1, is %d" % sum(data))

# Method 2 - use a loop
MySum=0
for item in data:
    MySum+=item

print("Sum, using method 2, is %d" % MySum)
