# Objective is to print the 1st, 3rd and last item from a list

# Store the numbers in a list
data=[1,-8,2,-6,3]

# Use integer formating to print selected items:
print('First number is %d' % data[0])
print('Third number is %d' % data[2])
print('Last number is %d' % data[-1])
