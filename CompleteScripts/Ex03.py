# Objective: Ask user's age, then approximate their age in days

# Use raw_input to prompt user for input and save the answer in variable y
y=input('How old are you?\n')

# Convert the string inputed into a floating point number:
y=float(y)

# Estimate the number of days by multiplying by 365:
d=y*365

# Format the number of days as an integer and print to the screen:
print("You are at least %d days old" % (d))
