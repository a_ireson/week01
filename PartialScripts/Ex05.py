# Objective is to sum the numbers stored in a list

# Create the list
data=[???,-8,2,???,3]

# Print the sum of the list:

# Method 1 - use the build in function "sum"
print("Sum, using method 1, is %d" % sum(???))

# Method 2 - use a loop
MySum=???
for item in data:
    MySum+=item

print("Sum, using method 2, is %d" % ???)
