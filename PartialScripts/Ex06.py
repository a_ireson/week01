# Objective is to sum the absolute values of numbers stored in a list

# Create the list
data=[???,-8,2,???,3]

# Use a loop to sum the absolute values into MySum, one item at a time
# Initialize the sum to zero.
MySum=???
# Loop through all the items in data:
for item in data:
    # Add the absolute value to MySum
    MySum+=abs(???)

# Print the sum of the list:
print("Sum of absolute values is %d" % ???)
